(window["webpackJsonp"]=window["webpackJsonp"]||[]).push([[8],{"05ff":function(e,t,i){"use strict";i.r(t);var n=function(){var e=this,t=e.$createElement,i=e._self._c||t;return i("q-page",{attrs:{padding:""}},[e.$apollo.loading?i("div",{staticStyle:{width:"100%",height:"50%","text-align":"center","vertical-align":"middle","z-index":"999",color:"grey",position:"absolute"}},[i("div",{staticStyle:{top:"40%",width:"100%",height:"100%",position:"absolute"}},[i("q-spinner-gears",{attrs:{color:"grey",size:"4em"}}),i("div",{staticStyle:{margin:"1em"}},[e._v("Φόρτωση...")])],1)]):e._e(),e.rowLoading?i("div",{staticStyle:{width:"100%",height:"50%","text-align":"center","vertical-align":"middle","z-index":"999",color:"grey",position:"absolute"}},[i("div",{staticStyle:{top:"40%",width:"100%",height:"100%",position:"absolute"}},[i("q-spinner-gears",{attrs:{color:"grey",size:"4em"}}),i("div",{staticStyle:{margin:"1em"}},[e._v("Φόρτωση...")])],1)]):e._e(),e.$apollo.loading||e.rowLoading?e._e():i("div",{staticClass:"q-pa-xs"},[i("q-table",{staticClass:"my-sticky-column-table",attrs:{title:"Είδη",data:e.species,columns:e.columns,"row-key":"name","wrap-cells":""},scopedSlots:e._u([{key:"top",fn:function(t){return[i("div",{staticClass:"col-2 q-table__title"},[e._v("Είδη")]),i("q-space"),i("q-btn",{staticClass:"q-ml-xs",attrs:{dense:"",icon:t.inFullscreen?"fullscreen_exit":"fullscreen"},on:{click:t.toggleFullscreen}}),i("q-btn",{staticClass:"q-ml-xs",attrs:{dense:"",icon:"add"},on:{click:function(t){e.addRowDialog=!0}}}),e.rowsChanged.length>0?i("q-btn",{staticClass:"q-ml-xs",attrs:{dense:"",color:"primary",icon:"save"},on:{click:e.saveTable}}):e._e()]}},{key:"body",fn:function(t){return[i("q-tr",{attrs:{props:t}},[i("q-td",{key:"id",attrs:{props:t}},[i("q-btn",{attrs:{icon:"delete",dense:"",flat:"",color:"negative"},on:{click:function(i){return e.deleteSpecies(t.row.id)}}})],1),i("q-td",{key:"species",attrs:{props:t}},[e._v("\n          "+e._s(t.row.species)+"\n          "),i("q-popup-edit",{model:{value:t.row.species,callback:function(i){e.$set(t.row,"species",i)},expression:"props.row.species"}},[i("q-input",{attrs:{dense:"",autofocus:"",counter:""},on:{input:function(i){return e.trackRowChanges(t.row)}},model:{value:t.row.species,callback:function(i){e.$set(t.row,"species",i)},expression:"props.row.species"}})],1)],1),i("q-td",{key:"family",attrs:{props:t}},[e._v("\n          "+e._s(t.row.family)+"\n          "),i("q-popup-edit",{model:{value:t.row.family,callback:function(i){e.$set(t.row,"family",i)},expression:"props.row.family"}},[i("q-input",{attrs:{dense:"",autofocus:"",counter:""},on:{input:function(i){return e.trackRowChanges(t.row)}},model:{value:t.row.family,callback:function(i){e.$set(t.row,"family",i)},expression:"props.row.family"}})],1)],1),i("q-td",{key:"classification",attrs:{props:t}},[e._v("\n          "+e._s(t.row.classification)+"\n          "),i("q-popup-edit",{model:{value:t.row.classification,callback:function(i){e.$set(t.row,"classification",i)},expression:"props.row.classification"}},[i("q-input",{attrs:{dense:"",autofocus:"",counter:""},on:{input:function(i){return e.trackRowChanges(t.row)}},model:{value:t.row.classification,callback:function(i){e.$set(t.row,"classification",i)},expression:"props.row.classification"}})],1)],1),i("q-td",{key:"iunc",attrs:{props:t}},[e._v("\n          "+e._s(t.row.iunc)+"\n          "),i("q-popup-edit",{model:{value:t.row.iunc,callback:function(i){e.$set(t.row,"iunc",i)},expression:"props.row.iunc"}},[i("q-input",{attrs:{dense:"",autofocus:"",counter:""},on:{input:function(i){return e.trackRowChanges(t.row)}},model:{value:t.row.iunc,callback:function(i){e.$set(t.row,"iunc",i)},expression:"props.row.iunc"}})],1)],1),i("q-td",{key:"directive_92",attrs:{props:t}},[e._v("\n          "+e._s(t.row.directive_92)+"\n          "),i("q-popup-edit",{model:{value:t.row.directive_92,callback:function(i){e.$set(t.row,"directive_92",i)},expression:"props.row.directive_92"}},[i("q-input",{attrs:{dense:"",autofocus:"",counter:""},on:{input:function(i){return e.trackRowChanges(t.row)}},model:{value:t.row.directive_92,callback:function(i){e.$set(t.row,"directive_92",i)},expression:"props.row.directive_92"}})],1)],1),i("q-td",{key:"bern",attrs:{props:t}},[e._v("\n          "+e._s(t.row.bern)+"\n          "),i("q-popup-edit",{model:{value:t.row.bern,callback:function(i){e.$set(t.row,"bern",i)},expression:"props.row.bern"}},[i("q-input",{attrs:{dense:"",autofocus:"",counter:""},on:{input:function(i){return e.trackRowChanges(t.row)}},model:{value:t.row.bern,callback:function(i){e.$set(t.row,"bern",i)},expression:"props.row.bern"}})],1)],1),i("q-td",{key:"cites",attrs:{props:t}},[e._v("\n          "+e._s(t.row.cites)+"\n          "),i("q-popup-edit",{model:{value:t.row.cites,callback:function(i){e.$set(t.row,"cites",i)},expression:"props.row.cites"}},[i("q-input",{attrs:{dense:"",autofocus:"",counter:""},on:{input:function(i){return e.trackRowChanges(t.row)}},model:{value:t.row.cites,callback:function(i){e.$set(t.row,"cites",i)},expression:"props.row.cites"}})],1)],1),i("q-td",{key:"date",attrs:{props:t}},[e._v("\n          "+e._s(t.row.date)+"\n          "),i("q-popup-edit",{model:{value:t.row.date,callback:function(i){e.$set(t.row,"date",i)},expression:"props.row.date"}},[i("q-input",{attrs:{dense:"",autofocus:"",counter:""},on:{input:function(i){return e.trackRowChanges(t.row)}},model:{value:t.row.date,callback:function(i){e.$set(t.row,"date",i)},expression:"props.row.date"}})],1)],1),i("q-td",{key:"source",attrs:{props:t}},[e._v("\n          "+e._s(t.row.source)+"\n          "),i("q-popup-edit",{model:{value:t.row.source,callback:function(i){e.$set(t.row,"source",i)},expression:"props.row.source"}},[i("q-input",{attrs:{dense:"",autofocus:"",counter:""},on:{input:function(i){return e.trackRowChanges(t.row)}},model:{value:t.row.source,callback:function(i){e.$set(t.row,"source",i)},expression:"props.row.source"}})],1)],1),i("q-td",{key:"eunis",attrs:{props:t}},[e._v("\n          "+e._s(t.row.eunis)+"\n          "),i("q-popup-edit",{model:{value:t.row.eunis,callback:function(i){e.$set(t.row,"eunis",i)},expression:"props.row.eunis"}},[i("q-input",{attrs:{dense:"",autofocus:"",counter:""},on:{input:function(i){return e.trackRowChanges(t.row)}},model:{value:t.row.eunis,callback:function(i){e.$set(t.row,"eunis",i)},expression:"props.row.eunis"}})],1)],1)],1)]}}],null,!1,1347224046)}),i("q-dialog",{attrs:{persistent:"","transition-show":"scale","transition-hide":"scale"},model:{value:e.addRowDialog,callback:function(t){e.addRowDialog=t},expression:"addRowDialog"}},[e.rowLoading?i("div",{staticStyle:{width:"100%",height:"100vh","text-align":"center","vertical-align":"middle","z-index":"999",color:"grey",position:"absolute"}},[i("div",{staticStyle:{top:"40%",width:"100%",height:"100vh",position:"absolute"}},[i("q-spinner-gears",{attrs:{color:"grey",size:"4em"}}),i("div",{staticStyle:{margin:"1em"}},[e._v("Φόρτωση...")])],1)]):e._e(),i("q-card",{staticStyle:{width:"50%"}},[i("q-card-section",[i("div",{staticClass:"text-h6"},[e._v("Προσθήκη")])]),i("q-card-section",{staticClass:"q-pt-none"},[i("q-input",{attrs:{dense:"",label:"Είδος"},model:{value:e.new_species,callback:function(t){e.new_species=t},expression:"new_species"}}),i("q-input",{attrs:{dense:"",label:"Οικογένεια"},model:{value:e.new_family,callback:function(t){e.new_family=t},expression:"new_family"}}),i("q-input",{attrs:{dense:"",label:"Τάξη"},model:{value:e.new_classification,callback:function(t){e.new_classification=t},expression:"new_classification"}}),i("q-input",{attrs:{dense:"",label:"Κωδικός Ν2Κ"},model:{value:e.new_n2k,callback:function(t){e.new_n2k=t},expression:"new_n2k"}}),i("q-input",{attrs:{dense:"",label:"Περιοχή Ν2Κ"},model:{value:e.new_code_n2k,callback:function(t){e.new_code_n2k=t},expression:"new_code_n2k"}}),i("q-input",{attrs:{dense:"",label:"IUNC"},model:{value:e.new_iunc,callback:function(t){e.new_iunc=t},expression:"new_iunc"}}),i("q-input",{attrs:{dense:"",label:"Οδηγία 92/43/ΕΟΚ"},model:{value:e.new_directive_92,callback:function(t){e.new_directive_92=t},expression:"new_directive_92"}}),i("q-input",{attrs:{dense:"",label:"Σύμβαση Βέρνης"},model:{value:e.new_bern,callback:function(t){e.new_bern=t},expression:"new_bern"}}),i("q-input",{attrs:{dense:"",label:"Σύμβαση Βόννης"},model:{value:e.new_bonn,callback:function(t){e.new_bonn=t},expression:"new_bonn"}}),i("q-input",{attrs:{dense:"",label:"Cites"},model:{value:e.new_cites,callback:function(t){e.new_cites=t},expression:"new_cites"}}),i("q-input",{attrs:{dense:"",label:"Ημερομηνία"},model:{value:e.new_date,callback:function(t){e.new_date=t},expression:"new_date"}}),i("q-input",{attrs:{dense:"",label:"Πηγή"},model:{value:e.new_source,callback:function(t){e.new_source=t},expression:"new_source"}}),i("q-input",{attrs:{dense:"",label:"Σύνδεσμος EUNIS"},model:{value:e.new_eunis,callback:function(t){e.new_eunis=t},expression:"new_eunis"}})],1),i("q-card-actions",{attrs:{align:"right"}},[i("q-btn",{attrs:{label:"Αποθήκευση",color:"primary"},on:{click:function(t){return e.addSpecies()}}}),i("q-btn",{directives:[{name:"close-popup",rawName:"v-close-popup"}],attrs:{outline:"",label:"Άκυρο"}})],1)],1)],1)],1)])},a=[],s=(i("ddb0"),i("1fee")),r={name:"DatabaseAdministration",apollo:{species:{query:s["b"].getSpecies,error(e){this.error=JSON.stringify(e.message)}}},methods:{saveTable(){this.rowLoading=!0,this.rowsChanged.forEach((e=>{this.$apollo.mutate({mutation:s["a"].updateSpecies,variables:{id:e.id,species:e.species,family:e.family,classification:e.classification,n2k:e.n2k,code_n2k:e.code_n2k,iunc:e.iunc,directive_92:e.directive_92,bern:e.bern,bonn:e.bonn,cites:e.cites,date:e.date,source:e.source,eunis:e.eunis},update:(e,{data:{updateSpecies:t}})=>{this.rowLoading=!1}})}))},addSpecies(){const e=this.new_species,t=this.new_family,i=this.new_classification,n=this.new_n2k,a=this.new_code_n2k,r=this.new_iunc,o=this.new_directive_92,l=this.new_bern,c=this.new_bonn,d=this.new_cites,u=this.new_date,p=this.new_source,m=this.new_eunis;this.rowLoading=!0,this.$apollo.mutate({mutation:s["a"].addSpecies,variables:{species:e,family:t,classification:i,n2k:n,code_n2k:a,iunc:r,directive_92:o,bern:l,bonn:c,cites:d,date:u,source:p,eunis:m},update:(e,{data:{addSpecies:t}})=>{if(t){const i=e.readQuery({query:s["b"].getSpecies});i.species=[t,...i.species],e.writeQuery({query:s["b"].getSpecies,data:i})}this.addRowDialog=!1,this.rowLoading=!1}})},deleteSpecies(e){this.rowLoading=!0,this.$apollo.mutate({mutation:s["a"].deleteSpecies,variables:{id:e},update:(t,{data:{deleteSpecies:i}})=>{if(i){console.log(i);const n=t.readQuery({query:s["b"].getSpecies});n.species=n.species.filter((t=>t.id!==e)),t.writeQuery({query:s["b"].getSpecies,data:n})}this.rowLoading=!1}})},trackRowChanges(e){const t=[e];this.rowsChanged.some((t=>t.id===e.id))?this.rowsChanged=this.rowsChanged.map((e=>t.find((t=>t.id===e.id))||e)):this.rowsChanged.push(e)}},data(){return{addRowDialog:!1,rowLoading:!1,rowsChanged:[],new_species:null,new_family:null,new_classification:null,new_n2k:null,new_code_n2k:null,new_iunc:null,new_directive_92:null,new_bern:null,new_bonn:null,new_cites:null,new_date:null,new_source:null,new_eunis:null,columns:[{name:"id",required:!1,label:"",align:"left",field:e=>e.id},{name:"species",align:"center",label:"Είδος",field:"species",sortable:!0},{name:"family",label:"Οικογένεια",field:"family",sortable:!0},{name:"classification",label:"Τάξη",field:"classification",sortable:!0},{name:"iunc",label:"IUNC",field:"iunc",sortable:!0},{name:"directive_92",label:"Οδηγία 92/43/ΕΟΚ",field:"directive_92",sortable:!0},{name:"bern",label:"Σύμβαση Βέρνης",field:"bern",sortable:!0},{name:"cites",label:"Cites",field:"cites",sortable:!0},{name:"date",label:"Ημερομηνία",field:"date",sortable:!0},{name:"source",label:"Πηγή",field:"source",sortable:!0},{name:"eunis",label:"Σύνδεσμος EUNIS",field:"eunis",sortable:!0}]}}},o=r,l=i("2877"),c=i("9989"),d=i("cf57"),u=i("eaac"),p=i("2c91"),m=i("9c40"),b=i("bd08"),$=i("db86"),f=i("42a1"),w=i("27f9"),g=i("24e8"),_=i("f09f"),y=i("a370"),v=i("4b7e"),S=i("7f67"),k=i("eebe"),h=i.n(k),q=Object(l["a"])(o,n,a,!1,null,null,null);t["default"]=q.exports;h()(q,"components",{QPage:c["a"],QSpinnerGears:d["a"],QTable:u["a"],QSpace:p["a"],QBtn:m["a"],QTr:b["a"],QTd:$["a"],QPopupEdit:f["a"],QInput:w["a"],QDialog:g["a"],QCard:_["a"],QCardSection:y["a"],QCardActions:v["a"]}),h()(q,"directives",{ClosePopup:S["a"]})},"1fee":function(e,t,i){"use strict";i.d(t,"b",(function(){return n})),i.d(t,"a",(function(){return a}));var n={};i.r(n),i.d(n,"getLayers",(function(){return w})),i.d(n,"getPublishedLayers",(function(){return g})),i.d(n,"getSpecies",(function(){return _})),i.d(n,"getUsers",(function(){return y})),i.d(n,"getContacts",(function(){return v})),i.d(n,"getDepartments",(function(){return S})),i.d(n,"getUsersOfDepartment",(function(){return k})),i.d(n,"queryXY",(function(){return h}));var a={};i.r(a),i.d(a,"createUser",(function(){return F})),i.d(a,"updateUser",(function(){return J})),i.d(a,"deleteUser",(function(){return M})),i.d(a,"createDocument",(function(){return X})),i.d(a,"deleteLayer",(function(){return Y})),i.d(a,"createDepartment",(function(){return A})),i.d(a,"updateDepartment",(function(){return G})),i.d(a,"updateDepartmentManager",(function(){return H})),i.d(a,"deleteDepartment",(function(){return K})),i.d(a,"singleUpload",(function(){return V})),i.d(a,"signIn",(function(){return W})),i.d(a,"addLayer",(function(){return Z})),i.d(a,"addSpecies",(function(){return ee})),i.d(a,"updateSpecies",(function(){return te})),i.d(a,"deleteSpecies",(function(){return ie})),i.d(a,"updateLayer",(function(){return ne}));var s=i("5184");let r,o=e=>e;Object(s["a"])(r||(r=o`
  type Document {
    title: String!
  }
  type Mutation {
    addDocument(text: String!): Document
    editDocument(text: String!, id: String!): Document
  }
  type RoleInput {
    label: String
    value: String
  }
  type DepartmentInput {
    _id: ID
    title: String
    description: String
  }
`));let l,c,d,u,p,m,b,$,f=e=>e;const w=Object(s["a"])(l||(l=f`
  query getLayers {
    layers {
      id
      label
      details
      timespan
      filetype
      crs
      attributions
      filename
      crossorigin
      preload
      type
      url
      visible
      published
      zindex
      apikey
      dbtablename
    }
  }
`)),g=Object(s["a"])(c||(c=f`
  query getPublishedLayers {
    publishedLayers {
      id
      label
      details
      timespan
      filetype
      crs
      attributions
      filename
      crossorigin
      preload
      type
      url
      visible
      zindex
      apikey
      dbtablename
    }
  }
`)),_=Object(s["a"])(d||(d=f`
  query getSpecies {
    species {
      id
      species
      family
      classification
      n2k
      code_n2k
      iunc
      directive_92
      bern
      bonn
      cites
      date
      source
      eunis
    }
  }
`)),y=Object(s["a"])(u||(u=f`
  query getUsers {
    users {
      id
      username
      fullname
      password
      department {
        _id
        title
      }
      email
    }
  }
`)),v=Object(s["a"])(p||(p=f`
  query getContacts {
    contacts {
      _id
      fullname
      organization
      street
      streetNumber
      postalCode
      area
      city
      website
      email
      position
      department
      phone
      fax
      country
      region
      prefecture
    }
  }
`)),S=Object(s["a"])(m||(m=f`
  query getDepartments {
    departments {
      _id
      title
      description
      manager
    }
  }
`)),k=Object(s["a"])(b||(b=f`
  query usersByDepartment($_id: ID) {
    usersByDepartment(_id: $_id) {
      _id
      username
      fullname
      password
      department {
        _id
        title
        description
      }
      email
    }
  }
`)),h=Object(s["a"])($||($=f`
  query queryXY($layer: String, $type: String, $zoom: Int, $x: Float, $y: Float){
    queryXY(layer: $layer, type: $type, zoom: $zoom, x: $x, y: $y)
  }
`));i("ffd6");let q,x,D,C,I,O,j,L,z,R,Q,U,T,B,N,E,P=e=>e;const F=Object(s["a"])(q||(q=P`
  mutation(
    $username: String
    $fullname: String
    $password: String
    $department: [ID]
    $email: String
  ) {
    createUser(
      username: $username
      fullname: $fullname
      password: $password
      department: $department
      email: $email
    ) {
      _id
      username
      fullname
      password
      department {
        _id
        title
        description
      }
      email
    }
  }
`)),J=Object(s["a"])(x||(x=P`
  mutation(
    $_id: ID!
    $username: String
    $fullname: String
    $password: String
    $department: [ID]
    $email: String
  ) {
    updateUser(
      _id: $_id
      username: $username
      fullname: $fullname
      password: $password
      department: $department
      email: $email
    ) {
      username
      _id
    }
  }
`)),M=Object(s["a"])(D||(D=P`
  mutation($_id: ID!) {
    deleteUser(_id: $_id)
  }
`)),X=Object(s["a"])(C||(C=P`
  mutation(
    $title: String!
    $description: String
    $sender: ID!
    $receivers: [ID!]
    $date: Date
    $files: [ID]
    $transactionType: String
    $timestamp: String
  ) {
    createDocument(
      title: $title
      description: $description
      sender: $sender
      receivers: $receivers
      date: $date
      files: $files
      transactionType: $transactionType
      timestamp: $timestamp
    ) {
      _id
      title
      description
      sender
      receivers
      date
      files
      transactionType
      timestamp
    }
  }
`)),Y=Object(s["a"])(I||(I=P`
  mutation($id: String!) {
    deleteLayer(id: $id)
  }
`)),A=Object(s["a"])(O||(O=P`
  mutation($title: String, $description: String) {
    createDepartment(title: $title, description: $description) {
      _id
      title
      description
      manager
    }
  }
`)),G=Object(s["a"])(j||(j=P`
  mutation($_id: ID!, $title: String, $description: String, $manager: ID) {
    updateDepartment(
      _id: $_id
      title: $title
      description: $description
      manager: $manager
    ) {
      _id
      title
      description
      manager
    }
  }
`)),H=Object(s["a"])(L||(L=P`
  mutation($_id: ID!, $manager: ID) {
    updateDepartmentManager(_id: $_id, manager: $manager) {
      _id
      manager
    }
  }
`)),K=Object(s["a"])(z||(z=P`
  mutation($_id: ID!) {
    deleteDepartment(_id: $_id)
  }
`)),V=Object(s["a"])(R||(R=P`
  mutation singleUpload($doc_id: ID!, $file: Upload!) {
    singleUploadLocal(doc_id: $doc_id, file: $file) {
      filename
      encoding
      mimetype
      _id
    }
  }
`)),W=Object(s["a"])(Q||(Q=P`
  mutation signIn($login: String!, $password: String!) {
    signIn(login: $login, password: $password) {
      token
      user_id
      username
      email
      role
    }
  }
`)),Z=Object(s["a"])(U||(U=P`
  mutation addLayer(
    $label: String,
    $details: String,
    $timespan: String,
    $filetype: String,
    $crs: String,
    $attributions: String,
    $filename: String,
    $type: String,
    $url: String,
    $visible: Boolean,
    $published: Boolean,
    $apikey: String,
    $dbtablename: String,
    $zindex: Int
  )
  {
    addLayer(
      label: $label
      details: $details
      timespan: $timespan
      filetype: $filetype
      crs: $crs
      attributions: $attributions
      filename: $filename
      type: $type
      url: $url
      visible: $visible
      published: $published
      apikey: $apikey
      dbtablename: $dbtablename
      zindex: $zindex
    ) {
      id
      label
    }
  }
`)),ee=Object(s["a"])(T||(T=P`
  mutation addSpecies(
    $species: String
    $family: String
    $classification: String
    $n2k: String
    $code_n2k: String
    $iunc: String
    $directive_92: String
    $bern: String
    $bonn: String
    $cites: String
    $date: String
    $source: String
    $eunis: String
  )
  {
    addSpecies(
      species: $species
      family: $family
      classification: $classification
      n2k: $n2k
      code_n2k: $code_n2k
      iunc: $iunc
      directive_92: $directive_92
      bern: $bern
      bonn: $bonn
      cites: $cites
      date: $date
      source: $source
      eunis: $eunis
    ) {
      id
      species
      family
      classification
      n2k
      code_n2k
      iunc
      directive_92
      bern
      bonn
      cites
      date
      source
      eunis
    }
  }
`)),te=Object(s["a"])(B||(B=P`
  mutation(
    $id: String
    $species: String
    $family: String
    $classification: String
    $n2k: String
    $code_n2k: String
    $iunc: String
    $directive_92: String
    $bern: String
    $bonn: String
    $cites: String
    $date: String
    $source: String
    $eunis: String
  ) {
    updateSpecies(
      id: $id
      species: $species
      family: $family
      classification: $classification
      n2k: $n2k
      code_n2k: $code_n2k
      iunc: $iunc
      directive_92: $directive_92
      bern: $bern
      bonn: $bonn
      cites: $cites
      date: $date
      source: $source
      eunis: $eunis
    ) {
      id
    }
  }
`)),ie=Object(s["a"])(N||(N=P`
  mutation($id: String) {
    deleteSpecies(id: $id)
  }
`)),ne=Object(s["a"])(E||(E=P`
  mutation updateLayer(
    $id: String,
    $label: String,
    $details: String,
    $timespan: String,
    $filetype: String,
    $crs: String,
    $attributions: String,
    $filename: String,
    $type: String,
    $url: String,
    $visible: Boolean,
    $published: Boolean,
    $apikey: String,
    $dbtablename: String,
    $zindex: Int
  )
  {
    updateLayer(
      id: $id
      label: $label
      details: $details
      timespan: $timespan
      filetype: $filetype
      crs: $crs
      attributions: $attributions
      filename: $filename
      type: $type
      url: $url
      visible: $visible
      published: $published
      apikey: $apikey
      dbtablename: $dbtablename
      zindex: $zindex
    ) {
      id
      label
      details
      timespan
      filetype
      crs
      attributions
      filename
      type
      url
      visible
      published
      apikey
      dbtablename
      zindex
    }
  }
`))},ffd6:function(e,t){}}]);